class GenericSearch {
  constructor(init, expand) {
    this.states = Array.isArray(init) ? init : [init];
    this.expand = expand;
  }
  searchFirst() {
    while (this.states.length > 0) {
      let state = this.states.pop();
      if (state.isSolution()) {
        return state;
      }
      this.expand.apply(this.states, state.makeChildren());
    }
  }
  searchAll() {
    let solutions = [];
    while (this.states.length > 0) {
      let state = this.states.pop();
      if (state.isSolution()) {
        solutions.push(state);
      } else {
        this.expand.apply(this.states, state.makeChildren());
      }
    }
    return solutions;
  }
}
module.exports = GenericSearch;
