const BreadthSearch = require('./BreadthSearch');
const DepthSearch = require('./DepthSearch');

module.exports = {
  BreadthSearch: BreadthSearch,
  DepthSearch: DepthSearch
};