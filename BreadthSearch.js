const GenericSearch = require("./GenericSearch");
class BreadthSearch extends GenericSearch {
  constructor(init) {
    super(init, Array.prototype.push);
  }
}
module.exports = BreadthSearch;
