const GenericSearch = require("./GenericSearch");
class DepthSearch extends GenericSearch {
  constructor(init) {
    super(init, Array.prototype.unshift);
  }
}
module.exports = DepthSearch;
